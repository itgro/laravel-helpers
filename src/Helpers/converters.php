<?php

if (!function_exists('string_to_float')) {
    function string_to_float(string $amount): float
    {
        return (float)preg_replace('~[^-+\d.]~', '', str_replace(',', '.', $amount));
    }
}

if (!function_exists('string_to_money')) {
    function string_to_money(string $value): int
    {
        return (int)round(string_to_float($value) * 100);
    }
}
