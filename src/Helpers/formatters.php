<?php

use Carbon\CarbonInterface;

if (!function_exists('nbsp')) {
    function nbsp(string $string): string
    {
        return preg_replace('/ +/', '&nbsp;', $string);
    }
}

if (!function_exists('format_date')) {
    function format_date(?CarbonInterface $date, string $pattern)
    {
        if (is_null($date)) {
            return '';
        }

        $formatter = new IntlDateFormatter(
            config('app.locale'),
            IntlDateFormatter::FULL,
            IntlDateFormatter::FULL,
            config('app.timezone'),
            IntlDateFormatter::GREGORIAN,
            $pattern
        );

        return $formatter->format($date->timestamp);
    }
}

if (!function_exists('pretty_month')) {
    function pretty_month(?CarbonInterface $date): string
    {
        return format_date($date, 'LLLL y г.');
    }
}

if (!function_exists('pretty_date')) {
    function pretty_date(?CarbonInterface $date, $includeTime = false): string
    {
        return format_date($date, 'd MMMM y г.' . ($includeTime ? ' H:mm' : ''));
    }
}

if (!function_exists('format_money')) {
    function format_money(int $amount, $includeCents = true, $includeCurrency = true): string
    {
        $formatter = new NumberFormatter(config('app.locale'), NumberFormatter::CURRENCY);

        $formatter->setTextAttribute(NumberFormatter::CURRENCY_CODE, 'RUR');
        $formatter->setAttribute(NumberFormatter::FRACTION_DIGITS, $includeCents ? 2 : 0);

        if (!$includeCurrency) {
            $formatter->setSymbol(NumberFormatter::CURRENCY_SYMBOL, '');
        }

        return $formatter->format($amount / 100);
    }
}

if (!function_exists('choose_declension')) {
    /**
     * @param int|float $num Число, на основании которого выбирается склонение
     * @param string $one Склонение для числа 1 (ex: штука)
     * @param string $three Склонение для числа 3 (ex: штуки)
     * @param string $five Склонение для числа 5 (ex: штук)
     *
     * @return string
     */
    function choose_declension($num, string $one = 'штука', string $three = 'штуки', string $five = 'штук'): string
    {
        if (is_float($num)) {
            return $three;
        }

        $num %= 100;

        if ($num >= 11 && $num <= 19) {
            return $five;
        }

        $num %= 10;

        if ($num === 1) {
            return $one;
        }

        if ($num > 0 && $num <= 4) {
            return $three;
        }

        return $five;
    }
}

if (!function_exists('money_as_words')) {
    /**
     * @link https://habrahabr.ru/post/53210/
     * @param int $amount
     * @return string
     */
    function money_as_words(int $amount)
    {
        $amount = $amount / 100;

        $nul = 'ноль';
        $ten = [
            ['', 'один', 'два', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'],
            ['', 'одна', 'две', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'],
        ];
        $a20 = ['десять', 'одиннадцать', 'двенадцать', 'тринадцать', 'четырнадцать', 'пятнадцать', 'шестнадцать', 'семнадцать', 'восемнадцать', 'девятнадцать'];
        $tens = [2 => 'двадцать', 'тридцать', 'сорок', 'пятьдесят', 'шестьдесят', 'семьдесят', 'восемьдесят', 'девяносто'];
        $hundred = ['', 'сто', 'двести', 'триста', 'четыреста', 'пятьсот', 'шестьсот', 'семьсот', 'восемьсот', 'девятьсот'];
        $unit = [
            ['копейка', 'копейки', 'копеек', 1],
            ['рубль', 'рубля', 'рублей', 0],
            ['тысяча', 'тысячи', 'тысяч', 1],
            ['миллион', 'миллиона', 'миллионов', 0],
            ['миллиард', 'милиарда', 'миллиардов', 0],
        ];

        list($rub, $kop) = explode('.', sprintf("%015.2f", floatval($amount)));

        $out = [];

        if (intval($rub) > 0) {
            foreach (str_split($rub, 3) as $uk => $v) { // by 3 symbols
                if (!intval($v)) {
                    continue;
                }
                $uk = sizeof($unit) - $uk - 1; // unit key
                $gender = $unit[$uk][3];
                list($i1, $i2, $i3) = array_map('intval', str_split($v, 1));
                // mega-logic
                $out[] = $hundred[$i1]; # 1xx-9xx
                if ($i2 > 1) {
                    $out[] = $tens[$i2] . ' ' . $ten[$gender][$i3];
                } # 20-99
                else {
                    $out[] = $i2 > 0 ? $a20[$i3] : $ten[$gender][$i3];
                } # 10-19 | 1-9
                // units without rub & kop
                if ($uk > 1) {
                    $out[] = choose_declension($v, $unit[$uk][0], $unit[$uk][1], $unit[$uk][2]);
                }
            }
        } else {
            $out[] = $nul;
        }

        $out[] = choose_declension(intval($rub), $unit[1][0], $unit[1][1], $unit[1][2]); // rub
        $out[] = $kop . ' ' . choose_declension($kop, $unit[0][0], $unit[0][1], $unit[0][2]); // kop

        return trim(preg_replace('/ {2,}/', ' ', join(' ', $out)));
    }
}
