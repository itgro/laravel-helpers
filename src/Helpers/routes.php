<?php

use Illuminate\Support\Arr;
use Illuminate\Support\HtmlString;
use Illuminate\Support\Str;

if (!function_exists('route_resource_with_middleware')) {
    function route_resource_with_middleware($path, $controller, $routesMiddleware): void
    {
        Route::resource($path, $controller);

        foreach ($routesMiddleware as $route => $middleware) {
            Route::resource($path, $controller, ['only' => $route])->middleware((array)$middleware);
        }
    }
}

if (!function_exists('link_back')) {
    function link_back(string $fallback = '/', string $text = 'Назад', array $attributes = [], ?bool $secure = null, ?bool $escape = null): HtmlString
    {
        $url = session()->previousUrl() ?: $fallback;

        if ($url === url()->current()) {
            $url = $fallback;
        }

        return link_to($url, $text, $attributes, $secure, $escape);
    }
}

if (!function_exists('link_to_external')) {
    function link_to_external(string $url, ?string $title = null, array $attributes = []): HtmlString
    {
        $title = $title ?: $url;

        $attributes['target'] = '_blank';
        $attributes['rel'] = 'nofollow';

        if (mb_substr($url, 0, mb_strlen('http')) !== 'http') {
            $url = 'http://' . $url;
        }

        return app('html')->link($url, $title, $attributes);
    }
}

if (!function_exists('phone_link')) {
    function phone_link(string $title): HtmlString
    {
        $number = preg_replace('/[^+0-9]/', '', $title);

        return app('html')->link("tel:{$number}", $title);
    }
}

if (!function_exists('phone_links')) {
    function phone_links($phones): string
    {
        $links = array_map(function (string $number) {
            return phone_link($number);
        }, array_values(Arr::wrap($phones)));

        return implode(', ', $links);
    }
}

if (!function_exists('utm_url')) {
    function utm_url(string $url, ?string $source = null, ?string $medium = null, ?string $campaign = null): string
    {
        $parameters = [];

        if (!is_null($source)) {
            $parameters['utm_source'] = $source;
        }

        if (!is_null($medium)) {
            $parameters['utm_medium'] = $medium;
        }

        if (!is_null($campaign)) {
            $parameters['utm_campaign'] = $campaign;
        }

        if (empty($parameters)) {
            return $url;
        }

        $prefix = Str::contains($url, '?') ? '&' : '?';

        return $url . $prefix . http_build_query($parameters);
    }
}

if (!function_exists('utm_email_url')) {
    function utm_email_url(string $url): string
    {
        return utm_url($url, 'email');
    }
}

if (!function_exists('add_query_params_to_url')) {
    function add_query_params_to_url(string $url, array $query = []): string
    {
        $parsedUrl = parse_url($url);

        if (!array_key_exists('path', $parsedUrl) || $parsedUrl['path'] === null) {
            $url .= '/';
        }

        $separator = (!array_key_exists('query', $parsedUrl) || $parsedUrl['query'] === null) ? '?' : '&';

        return $url . $separator . http_build_query($query);
    }
}
