<?php

if (!function_exists('execute_with_given_error_reporting_level')) {
    function execute_with_given_error_reporting_level(int $level, Closure $func)
    {
        $prevLevel = error_reporting();
        error_reporting($level);

        $result = $func();

        error_reporting($prevLevel);

        return $result;
    }
}

if (!function_exists('suppress_warnings')) {
    function suppress_warnings(Closure $func)
    {
        return execute_with_given_error_reporting_level(E_ERROR, $func);
    }
}

if (!function_exists('relative_class_path')) {
    function relative_class_path($namespace, $obj): string
    {
        return str_replace($namespace . '\\', '', get_class($obj));
    }
}
