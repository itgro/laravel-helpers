<?php

use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;

if (!function_exists('get_shard_path')) {
    /**
     * @param string|File|UploadedFile $filename
     * @return string
     */
    function get_shard_path($filename): string
    {
        if ($filename instanceof File || $filename instanceof UploadedFile) {
            $filename = $filename->hashName();
        }

        return trim(
            implode(DIRECTORY_SEPARATOR, [substr($filename, 0, 3), substr($filename, 3, 3)]),
            DIRECTORY_SEPARATOR
        );
    }
}
