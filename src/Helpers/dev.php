<?php

use Illuminate\Database\Query\Expression;
use Illuminate\Contracts\Debug\ExceptionHandler;

if (!function_exists('random_element_with_weight')) {
    /**
     * @param Faker\Generator|Faker\UniqueGenerator $faker
     * @param array $generatorsWithWeight
     * @return mixed
     */
    function random_faker_generator($faker, array $generatorsWithWeight)
    {
        $elements = [];

        foreach ($generatorsWithWeight as $generator => $weight) {
            for ($i = 0; $i < $weight; $i++) {
                $elements[] = $generator;
            }
        }

        return $faker->{array_random($elements)};
    }
}

if (!function_exists('create')) {
    function create(string $class, array $attributes = [], int $times = null)
    {
        return factory($class, $times)->create($attributes);
    }
}

if (!function_exists('make')) {
    function make(string $class, array $attributes = [], int $times = null)
    {
        return factory($class, $times)->make($attributes);
    }
}

if (!function_exists('make_db_if_statement')) {
    function make_db_if_statement($condition, $whenTrue, $whenFalse, ?string $connection = null): Expression
    {
        if (!$connection) {
            $connection = config('database.default');
        }

        $driver = config("database.connections.{$connection}.driver");

        if ($driver === 'sqlite') {
            $sql = "case when {$condition} then {$whenTrue} else {$whenFalse} end";
        } else {
            $sql = "IF({$condition}, {$whenTrue}, {$whenFalse})";
        }

        return DB::raw($sql);
    }
}

if (!function_exists('is_image')) {
    function is_image(string $path): bool
    {
        $imageInfo = @getimagesize($path);

        return (is_array($imageInfo) && !empty($imageInfo));
    }
}

if (!function_exists('check_js_file_validity')) {
    function check_js_file_validity(string $path): bool
    {
        $result = [];
        $exitCode = -1;

        exec(sprintf('node -c %s', $path), $result, $exitCode);

        return $exitCode === 0;
    }
}

if (!function_exists('report_exception')) {
    function report_exception(Exception $exception): void
    {
        /** @var ExceptionHandler $handler */
        $handler = app(ExceptionHandler::class);

        $handler->report($exception);
    }
}

if (!function_exists('array_map_with_keys')) {
    function array_map_with_keys(array $array, callable $callback): array
    {
        return array_map($callback, array_keys($array), $array);
    }
}
