<?php

namespace Itgro\LaravelHelpers;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    protected $defer = false;

    public function register()
    {
        foreach (glob(__DIR__ . '/Helpers/*.php') as $filename) {
            /** @noinspection PhpIncludeInspection */
            require_once($filename);
        }
    }
}
